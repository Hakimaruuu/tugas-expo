// NO 1
function teriak() {
    return 'Halo Sanbers!';
}

console.log(teriak());

// NO 2
function kalikan(x,y) {
    return x*y;
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);

console.log(hasilKali);

// NO 3
function introduce(a,b,c,d) {
    x = 'Nama saya '+a+', umur saya '+b+' tahun, alamat saya di '+c+', dan saya punya hobby yaitu '+d+'!';
    return x;
}
var name = 'Agus';
var age = '30';
var addr = 'Jln. Malioboro, Yogyakarta';
var hobby = 'Gaming';

var perkenalan = introduce(name,age,addr,hobby);
console.log(perkenalan);