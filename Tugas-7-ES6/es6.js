// NO 1
const golden = () => {
    console.log("this is golden!!");
}

golden();

// NO 2
const newFunction = (fn, ln) =>{
    return {
        firstName: fn,
        lastName : ln,
        fullName : function(){
            console.log(fn + " " + ln);
            return
        }
    }
}

const person = newFunction("William", "Imoh");
console.log(person);
person.fullName();  

/*
Output
{
  firstName: 'William',
  lastName: 'Imoh',
  fullName: [Function: fullName]
}
William Imoh
*/ 

// NO 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation)

// NO 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined);

// NO 5
const planet = "earth";
const view = "glass";
var before = 
    `Lorem ${view} dolor sit amet, 
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`;

// Driver Code
console.log(before)