// NO 1
function readBooks(time,book,callback) {
    console.log(`saya membaca ${book.name}`);
    setTimeout(()=>{
        let sisaWaktu = 0;
        if (time >= book.timeSpent){
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya tinggal ${sisaWaktu}`);
            callback(sisaWaktu);
        } else {
            console.log(`waktu saya habis`);
            callback(time);
        }
    }, book.timeSpent);
}
module.exports = readBooks;