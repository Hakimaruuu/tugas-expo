import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, Dimensions } from 'react-native'
import { FlatList } from 'react-native-gesture-handler';
import { Data }from './data'

const {height,width} = Dimensions.get('window');

export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);
    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
    }
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
            <FlatList
            columnWrapperStyle={{justifyContent: 'space-between'}}
                numColumns={2}
                data={Data}
                keyExtractor={(index, item) => index.toString()}
                renderItem={({index, item})=>{
                    return(
                        <View style={styles.content}>
                            <Text style={styles.tulisan1}>
                                {item.title}
                            </Text>
                            <Image source={item.image} style={styles.gambar} />
                            <Text style={styles.tulisan2}>
                                {item.desc}
                            </Text>
                            <Text style={styles.tulisan4}>
                                Rp. {item.harga}
                            </Text>
                            <Button color="cornflowerblue" title="BELI" onPress={()=> updateHarga(item.harga)} />
                        </View>
                    )
                }}
            />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
    tulisan1:{
        marginLeft: 10,
        fontSize: 14,
        fontStyle: 'normal',
        fontWeight: 'bold',
    },
    tulisan2:{
        marginLeft: 10,
        fontSize: 13,

    },
    tulisan4:{
        marginLeft: 10,
        fontSize: 13,
        fontWeight: 'bold',
    },
    gambar:{
        width: 100,
        height: 80,
    }
})
