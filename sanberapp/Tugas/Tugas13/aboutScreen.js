import { StatusBar } from 'expo-status-bar';
import React from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
} from 'react-native';

const {height,width} = Dimensions.get('window');

export default function aboutScreen() {
    return (
        <>
        <StatusBar style='light' translucent={false} backgroundColor={'#51a5f2'}/>
        <View style={styles.container}>
            <Text style={styles.headerText}>
                About Me
            </Text>
            <View style={styles.userImage}>
                <Image source={{uri : 'https://www.kindpng.com/picc/m/78-785827_user-profile-avatar-login-account-male-user-icon.png'}} style={styles.iconImage} />
            </View>
            <View style={styles.textContent}>
                <Text style={styles.mainText}>
                    Portofolio
                </Text>
            </View>
            <View style={styles.portofolio}>
                <View>
                    <Image source={{uri:'https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png'}} style={styles.imgContainter} />
                    <Text style={styles.textContainer}>
                        @Hakimaruuu
                    </Text>
                </View>
                <View>
                    <Image source={{uri:'https://images.tokopedia.net/img/cache/900/VqbcmM/2021/6/25/599b5cb6-7995-4773-a52a-507104ac881d.png'}} style={styles.imgContainter} />
                    <Text style={styles.textContainer}>
                        @Hakimaruuu
                    </Text>
                </View>
                <View>
                    <Image source={{uri:'https://inlab.fib.upc.edu/sites/default/files/styles/large/public/field/image/captura_3.png'}} style={styles.imgContainter} />
                    <Text style={styles.textContainer}>
                        rizkihakim723
                    </Text>
                </View>
            </View>
            <View style={styles.textContent}>
                <Text style={styles.mainText}>
                    Contacts
                </Text>
            </View>
            <View style={styles.contact}>
                <View>
                    <Image source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/512px-Telegram_logo.svg.png'}} style={styles.imgContainter} />
                    <Text style={styles.textContainer}>
                        Rizki Hakim
                    </Text>
                </View>
                <View>
                    <Image source={{uri:'https://www.kindpng.com/picc/m/49-493471_linkedin-logo-png-linkedin-logo-transparent-png.png'}} style={styles.imgContainter} />
                    <Text style={styles.textContainer}>
                        Rizki Amanullah H
                    </Text>
                </View>
                <View>
                    <Image source={{uri:'https://1.bp.blogspot.com/-gwkoWGuiuDE/YCfaKMVBvSI/AAAAAAAAD7c/_bnlcs6D4oIpQblR1UtbEkPwJqJ7mbxEQCLcBGAsYHQ/s1600/Logo%2BGmail%2BTerbaru.png'}} style={styles.imgContainter} />
                    <Text style={styles.textContainer}>
                        rizkiamanullah723
                    </Text>
                </View>
            </View>                 
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex: 1,
    },
    mainText:{
        color: 'black',
        fontSize: 18,
    },
    headerText:{
        color: 'black',
        fontSize:36,
        paddingLeft: width * 0.04,
        marginTop: height * 0.05,
    },
    textContent:{
        margin: 11,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgContainter:{
        height: 62,
        width: 60,
        margin: 12,
    },
    textContainer:{
        fontSize: 9,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: width * 0.025,
        marginTop: height * -0.01,
    },
    portofolio:{
        margin: 10,
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        borderStyle: 'solid',
        borderColor:'#DEDDDD',
        borderWidth: 1,
        borderRadius: 15,
    },
    contact:{
        margin: 10,
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        borderStyle: 'solid',
        borderColor:'#DEDDDD',
        borderWidth: 1,
        borderRadius: 15,
    },
    userImage:{
        marginTop: height * 0.01,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: height * 0.075,
    },
    iconImage:{
        height: 120,
        width: 120,
        resizeMode: 'contain',
        borderRadius: 80,
    },
});
