import { StatusBar } from 'expo-status-bar';
import React from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
} from 'react-native';

const {height,width} = Dimensions.get('window');

export default function skillsScreen() {
    return (
        <>
        <StatusBar style='light' translucent={false} backgroundColor={'#51a5f2'}/>
        <View style={styles.container}>
            <Text style={styles.headerText}>
                About Me
            </Text>
            <View style={styles.userImage}>
                <Image source={{uri : 'https://www.kindpng.com/picc/m/78-785827_user-profile-avatar-login-account-male-user-icon.png'}} style={styles.iconImage} />
            </View>
            <View style={styles.textContent}>
                <Text style={styles.mainText}>
                    Skills
                </Text>
            </View>
            <View style={styles.content}>
                <View style={styles.upper}>
                    <View>
                        <Image source={{uri:'https://cdn.pixabay.com/photo/2015/04/23/17/41/javascript-736400_960_720.png 1x, https://cdn.pixabay.com/photo/2015/04/23/17/41/javascript-736400_1280.png 2x'}} style={styles.imgContainter} />
                        <Text style={styles.textContainer}>
                            JavaScript
                        </Text>
                    </View>
                    <View>
                        <Image source={{uri:'https://www.tutorialkart.com/wp-content/uploads/2018/03/python.png'}} style={styles.imgContainter} />
                        <Text style={styles.textContainer}>
                            Python
                        </Text>
                    </View>
                    <View>
                        <Image source={{uri:'https://i.postimg.cc/90VzjmkD/ci4.png'}} style={styles.imgContainter} />
                        <Text style={styles.textContainer}>
                            CodeIgniter4
                        </Text>
                    </View>
                </View>
                <View style={styles.lower}>
                    <View>
                        <Image source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/220px-Laravel.svg.png'}} style={styles.imgContainter} />
                        <Text style={styles.textContainer}>
                            Laravel
                        </Text>
                    </View>
                    <View>
                        <Image source={{uri:'https://cdn2.downdetector.com/static/uploads/c/300/0d4f7/figma2.png'}} style={styles.imgContainter} />
                        <Text style={styles.textContainer}>
                            Figma
                        </Text>
                    </View>
                    <View>
                        <Image source={{uri:'https://logos-download.com/wp-content/uploads/2016/09/React_logo_logotype_emblem-700x626.png'}} style={styles.imgContainter} />
                        <Text style={styles.textContainer}>
                            React
                        </Text>
                    </View>
                </View>                 
            </View>
        </View>
        </>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex: 1,
    },
    mainText:{
        color: 'black',
        fontSize: 18,
    },
    headerText:{
        color: 'black',
        fontSize:36,
        paddingLeft: width * 0.04,
        marginTop: height * 0.05,
    },
    textContent:{
        margin: 11,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgContainter:{
        height: 72,
        width: 61,
        margin: 11,
    },
    textContainer:{
        fontSize: 9,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: width * 0.05,
        marginTop: height * -0.01,
    },
    content:{
        margin: 10,
        padding: 15,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        borderStyle: 'solid',
        borderColor:'#DEDDDD',
        borderWidth: 1,
        borderRadius: 15,
    },
    upper:{
        margin: 5,
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    lower:{
        margin: 5,
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    userImage:{
        marginTop: height * 0.01,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: height * 0.075,
    },
    iconImage:{
        height: 120,
        width: 120,
        resizeMode: 'contain',
        borderRadius: 80,
    },
});
