import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Text>
                LOGIN
            </Text>
            <Button color="cornflowerblue" title="Mulai disini" onPress={()=>navigation.navigate("MyDrawwer",{
                screen: 'App', params:{
                    screen: 'AboutScreen'
                }
            })} />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});