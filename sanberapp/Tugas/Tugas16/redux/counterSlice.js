import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    value:0,
};

export const counterSlice = createSlice({
    name:'counter',
    initialState,
    reducers:{
        increment: (state)=>{
            state.value+=1;
            console.log('increment')
        },
        decrement: (state)=>{
            state.value-=1;
            console.log('decrement')
        },
        incrementByAmount: (state)=>{
            state.value+=action.payload;
            console.log('byValue')
        },
    },
});

export const {increment,decrement,incrementByAmount}=counterSlice.actions;
export default counterSlice.reducer;