import React from 'react'
import Register from './screen/Register';
import Login from './screen/Login';
import Home from './screen/Home';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Welcome from './screen/Welcome';
import Details from './screen/Details';
import Splash from './screen/Splash';
import Settings from './screen/Settings';

const Stack = createStackNavigator();

export default function index() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name='Splash' component={Splash} options={{headerShown: false}}/>
                <Stack.Screen name='Welcome' component={Welcome} options={{headerShown: false}}/>
                <Stack.Screen name='Login' component={Login} options={{title: '', headerTintColor: '#fff', headerShown: true, headerTransparent: true}}/>
                <Stack.Screen name='Register' component={Register} options={{title: '', headerTintColor: '#fff', headerShown: true, headerTransparent: true}}/>
                <Stack.Screen name='Home' component={Home} options={{headerShown: false}}/>
                <Stack.Screen name='Details' component={Details} options={{title: '', headerTintColor: '#000', headerShown: true, headerTransparent: true}}/>
                <Stack.Screen name='Settings' component={Settings} options={{title: '', headerTintColor: '#000', headerShown: true, headerTransparent: true}}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}