import React from 'react'
import { View, Text } from 'react-native'

const ProductRow = ({title, price}) => (
    <View style={{ flexDirection: "row" }}>
            <Text>{title}</Text>
            <Text>${price}</Text>
    </View>
)

export default function index() {
    return (
        <>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        <ProductRow price = '99.99' title='iPodTouch'/>
        </>
    )
}
